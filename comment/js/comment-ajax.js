$( document ).ready(function() {


manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'api/comment/getData.php'
    }).done(function(data){

    	manageRow(data);

    });

}

/* Get Page Data*/
function getPageData() {
	$.ajax({
    	dataType: 'json',
    	url: 'http://localhost:8080/commentPart/comment.jsp'
	}).done(function(data){
		manageRow(data);
	});
}

/* Add new Item table row */
function manageRow(data) {
	var	rows = '';
	$.each( data, function( key, value ) {
	  	rows = rows + '<tr>';
	  	rows = rows + '<td>'+value.name_category+'</td>';
	  	rows = rows + '<td>'+value.description+'</td>';
	  	rows = rows + '<td data-id="'+value.id_category+'">';
        /*rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';*/
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';
	});

	$("tbody").html(rows);
}

/* Create new Item */
$(".crud-submit").click(function(e){
    e.preventDefault();
    var form_action = $("#create-item").find("form").attr("action");
    var name_category = $("#create-item").find("input[name='name_category']").val();
    var description = $("#create-item").find("input[name='description']").val();

    if(name_category != '' && description != ''){
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: form_action,
            data:{name_category:name_category, description:description}
        }).done(function(data){
            $("#create-item").find("input[name='name_category']").val('');
            $("#create-item").find("input[name='description']").val('');
            getPageData();
            $(".modal").modal('hide');
            toastr.success('Thank you for your comment!.', {timeOut: 5000});
        });
    }else{
        alert('You are missing title or description.')
    }

});

/* Remove Item */
$("body").on("click",".remove-item",function(){
    var id_category = $(this).parent("td").data('id');
    var c_obj = $(this).parents("tr");



    $.ajax({
        dataType: 'json',
        type:'GET',
        url: 'api/comment/delete.php',
        data:{id_category:id_category}
    }).done(function(data){
        c_obj.remove();
        toastr.success('Your comment has been deleted', {timeOut: 5000});
        getPageData();
    });

});


/* Updated new Item */

$(".crud-submit-edit").click(function(e){

    e.preventDefault();
    var form_action = $("#edit-item").find("form").attr("action");
    var name_category = $("#edit-item").find("input[name='name_category']").val();

    var description = $("#edit-item").find("textarea[name='description']").val();
    var id_category = $("#edit-item").find(".edit-id").val();

    if(name_category != '' && description != ''){
        $.ajax({
            dataType: 'json',
            type:'POST',
            url:  form_action,
            data:{name_category:name_category, description:description,id_category:id_category}
        }).done(function(data){
            getPageData();
            $(".modal").modal('hide');
            toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
        });
    }else{
        alert('You are missing title or description.')
    }

});
});